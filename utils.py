import scipy.misc

import pfm


def disp_from_png(png_path):
    """
    The dispaity is encoded in a RGB image. The functions returns the
    disparities.
    """
    data = scipy.misc.imread(png_path)
    disp = data[:, :, 0].astype('float32') * 4
    disp += data[:, :, 1].astype('float32') / (2 ** 6)
    disp += data[:, :, 2].astype('float32') / (2 ** 14)
    return disp


def load_disp(file_path):
    if file_path.endswith('.png'):
        return disp_from_png(file_path)
    if file_path.endswith('.pfm'):
        disp, _ = pfm.load_pfm(file_path)
        return disp
    raise ValueError('Invalid disp file')


def get_model_memory_usage(batch_size, model):
    import numpy as np
    from keras import backend as K

    shapes_mem_count = 0
    for l in model.layers:
        single_layer_mem = 1
        for s in l.output_shape:
            if s is None:
                continue
            single_layer_mem *= s
        shapes_mem_count += single_layer_mem

    trainable_count = np.sum([K.count_params(p)
                              for p in set(model.trainable_weights)])
    non_trainable_count = np.sum([K.count_params(p)
                                  for p in set(model.non_trainable_weights)])

    total_memory = 4.0 * batch_size
    total_memory *= (shapes_mem_count + trainable_count + non_trainable_count)
    gbytes = np.round(total_memory / (1024.0 ** 3), 3)
    return gbytes
