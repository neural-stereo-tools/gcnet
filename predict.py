import math

import click
import numpy as np
import scipy.misc

import pfm


@click.command()
@click.option('--show', default=True, type=click.BOOL)
@click.option('--caffe', type=click.BOOL, default=False)
@click.argument('weights_path', type=click.Path(exists=True, dir_okay=False))
@click.argument('left_img_path', type=click.Path(exists=True, dir_okay=False))
@click.argument('right_img_path', type=click.Path(exists=True, dir_okay=False))
@click.argument('target_path', type=click.Path(exists=False))
def run(show, caffe, weights_path,
        left_img_path, right_img_path, target_path):
    print('> load images')
    left = scipy.misc.imread(left_img_path)
    right = scipy.misc.imread(right_img_path)

    height, width, _ = left.shape
    divisor = 64
    adapted_width = math.ceil(width / divisor) * divisor
    adapted_height = math.ceil(height / divisor) * divisor

    left = scipy.misc.imresize(left, (adapted_height, adapted_width))
    right = scipy.misc.imresize(right, (adapted_height, adapted_width))

    if '.png' in left_img_path:
        # drop alpha channel
        left = left[:, :, :3]
        right = right[:, :, :3]

    if caffe:
        # convert RGB -> BGR
        left = left[:, :, ::-1]
        right = right[:, :, ::-1]
        # swap width and height
        left = left.transpose(1, 0, 2)
        right = right.transpose(1, 0, 2)

    # stack data
    input_data = np.concatenate((left, right), axis=2)
    input_data = np.expand_dims(input_data, axis=0)

    print('> load model')
    import dispnet
    model = dispnet.build_model()
    model.load_weights(weights_path)

    print ('> predict')
    disp = model.predict(input_data, batch_size=1)
    disp = np.squeeze(disp)

    if caffe:
        disp = disp.transpose(1, 0)
        disp = np.abs(disp)

    # upscale
    # NOTE: mode='F' keeps values as floats
    disp = scipy.misc.imresize(disp, (height, width), mode='F')
    if show:
        scipy.misc.toimage(disp).show()

    pfm.save_pfm(target_path, disp)


if __name__ == '__main__':
    run()
