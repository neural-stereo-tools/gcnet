import click
import scipy.misc
import sklearn.metrics
import numpy as np

import pfm


@click.command()
@click.option('--show', default=False, type=click.BOOL)
@click.argument('reference', type=click.Path(exists=True, dir_okay=False))
@click.argument('predicted', type=click.Path(exists=True, dir_okay=False))
def run(reference, predicted, show):
    ref, _ = pfm.load_pfm(reference)
    pred, _ = pfm.load_pfm(predicted)

    if show:
        scipy.misc.toimage(ref).show()
        scipy.misc.toimage(pred).show()

    print("Pixel count: {}".format(ref.size))
    print("Mean accuracy: {}".format(
        sklearn.metrics.mean_absolute_error(ref, pred)))
    print("Square error: {}".format(
        sklearn.metrics.mean_squared_error(ref, pred)))

    print("\nKITTY")
    ABS_THRESH = 3.0
    REL_THRESH = 0.05

    pixels_fg = (ref > 0).sum()
    pixels_bg = (ref == 0).sum()

    diff = np.abs(ref - pred)
    norm_disp = diff / np.abs(ref)

    errors = (diff > ABS_THRESH) & (norm_disp > REL_THRESH)
    errors_fg = ((ref > 0) & errors).sum()
    errors_bg = ((ref == 0) & errors).sum()
    errors = errors.sum()

    print("Error: {:.2f}%".format(errors / ref.size * 100))
    if errors_fg:
        print("Error (FG): {:.2f}%".format(errors_fg / pixels_fg * 100))
    if errors_bg:
        print("Error (BG): {:.2f}%".format(errors_bg / pixels_bg * 100))

    print('\nMIDDLEBURY')
    for thresh in [0.5, 1.0, 2.0, 3.0, 4.0]:
        errors = (diff > thresh).sum()
        print("bad {:1.1f}: {:.2f}%".format(
            thresh,
            errors / ref.size * 100,
        ))


if __name__ == '__main__':
    run()
