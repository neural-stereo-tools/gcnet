import glob
import os

import structures


def collect_images(path, validation=False):
    extension = 'webp'
    images = glob.glob(os.path.join(
        path, 'frames_cleanpass_webp', '*', '*', '*', 'left', '*.webp'
    ))
    if not images:
        # maybe png?
        extension = 'png'
        images = glob.glob(os.path.join(
            path, 'frames_cleanpass_webp', '*', '*', '*', 'left', '*.png'
        ))

    for img in images:
        if ((validation and 'TRAIN' in img) or
                (not validation and 'TEST' in img)):
            continue
        left_img = img
        right_img = img.replace('left', 'right')
        disp_img = img.replace('frames_cleanpass_webp', 'disparity')
        disp_img = disp_img.replace('.' + extension, '.pfm')

        if all(map(os.path.exists, [left_img, right_img, disp_img])):
            yield structures.StereoImageProxy(left_img, right_img, disp_img)

        left_img = left_img.replace('cleanpass', 'finalpass')
        right_img = right_img.replace('cleanpass', 'finalpass')

        if all(map(os.path.exists, [left_img, right_img, disp_img])):
            yield structures.StereoImageProxy(left_img, right_img, disp_img)
