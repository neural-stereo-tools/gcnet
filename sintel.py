import glob
import os
import re

import structures


def transform_disp(data):
    """
    The dispaity is encoded in a RGB image. The functions returns the
    disparities.
    """
    disp = data[:, :, 0].astype('float32') * 4
    disp += data[:, :, 1].astype('float32') / (2 ** 6)
    disp += data[:, :, 2].astype('float32') / (2 ** 14)
    return disp


def collect_images(path, validation=False):
    images = glob.glob(os.path.join(path, 'clean_left', '*', '*.png'))

    for img in images:
        img_num = int(re.search(r'(\d+).png', img).group(1))
        if ((validation and img_num <= 40) or  # data for validation
                (not validation and img_num > 40)):  # data for training
            continue

        left_img = img
        right_img = img.replace('left', 'right')
        disp_img = img.replace('clean_left', 'disparities')

        if all(map(os.path.exists, [left_img, right_img, disp_img])):
            yield structures.StereoImageProxy(left_img, right_img, disp_img,
                                              disp_transform=transform_disp)

        left_img = left_img.replace('clean', 'final')
        right_img = right_img.replace('clean', 'final')

        if all(map(os.path.exists, [left_img, right_img, disp_img])):
            yield structures.StereoImageProxy(left_img, right_img, disp_img,
                                              disp_transform=transform_disp)
