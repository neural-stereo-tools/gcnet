from functools import partial

import numpy as np
import keras.backend as K
from keras.activations import softmax
from keras.layers import Activation, Conv2D, Conv3D, Conv3DTranspose, Input, Lambda, ZeroPadding2D
from keras.layers.merge import Add
from keras.layers.normalization import BatchNormalization
from keras.models import Model
from keras.optimizers import RMSprop


def unary_features_block(input_layer):
    def conv2d_block(input, kernel, strides, activation=True):
        conv = Conv2D(32, kernel_size=(kernel, kernel), strides=strides,
                      activation=None, use_bias=True, padding='same')(input)
        if activation:
            bn = BatchNormalization()(conv)
            conv = Activation('relu')(bn)
        return conv

    def residual_block(input):
        conv = input
        for _ in range(2):
            conv = conv2d_block(conv, 3, 1)
        return Add()([conv, input])

    conv = conv2d_block(input_layer, 5, 2)
    for _ in range(8):
        conv = residual_block(conv)

    conv = conv2d_block(conv, 3, 1, activation=False)
    return conv


def cost_volume_block(height, width, max_disp):
    if max_disp % 2 != 0:
        raise ValueError(f"max_disp must be divisible by 2.")
    disp = int(max_disp / 2)

    def inner(inputs):
        left_tensor, right_tensor = inputs
        # extend right features by `disp` columns on the left side
        right_tensor = ZeroPadding2D(padding=((0, 0), (disp, 0)), dtype='float32')(right_tensor)
        shape = K.shape(left_tensor)

        disparity_costs = []
        for d in range(disp):
            # get slice with `d` 0 on the left -> shifting feature over each other
            left_tensor_slice = left_tensor
            right_tensor_slice = right_tensor[:, :, d: d + shape[2], :]
            cost = K.concatenate(
                [left_tensor_slice, right_tensor_slice],
                axis=3,
            )
            disparity_costs.append(cost)
        # stack everything into a 4D tensor
        return K.stack(disparity_costs, axis=1)

    return Lambda(
        inner,
        output_shape=(disp, int(height / 2), int(width / 2), 2 * 32),
    )


def regularization_block(cost_volume):
    def single_conv3d_block(input_layer, features=32, strides=1):
        conv = Conv3D(features, kernel_size=(3, 3, 3), dtype='float32',
                      strides=strides, padding='same')(input_layer)
        bn = BatchNormalization(dtype='float32')(conv)
        return Activation('relu')(bn)

    def conv3d_block(input_layer, features):
        conv = single_conv3d_block(input_layer, features)
        return single_conv3d_block(conv, features)

    def downsample_block(input_layer, features):
        conv = single_conv3d_block(input_layer, features, strides=2)
        return conv3d_block(conv, features)

    def upsample_block(input_layer, residual, features):
        conv = Conv3DTranspose(features, kernel_size=(3, 3, 3), dtype='float32',
                               strides=2, padding='same')(input_layer)
        bn = BatchNormalization(dtype='float32')(conv)
        relu = Activation('relu')(bn)
        return Add()([relu, residual])

    # downsample
    conv20 = conv3d_block(cost_volume, 32)
    conv23 = downsample_block(conv20, 64)
    conv26 = downsample_block(conv23, 64)
    conv29 = downsample_block(conv26, 64)
    conv32 = downsample_block(conv29, 128)

    # upsample
    conv33 = upsample_block(conv32, conv29, 64)
    conv34 = upsample_block(conv33, conv26, 64)
    conv35 = upsample_block(conv34, conv23, 64)
    conv36 = upsample_block(conv35, conv20, 32)

    # omit bn and relu!
    # output: batch x disp x height x width x 1
    conv37 = Conv3DTranspose(1, kernel_size=3, dtype='float32',
                             strides=2, padding='same')(conv36)

    return conv37


def soft_argmin(height, width, max_disp):
    def inner(cost):
        # there is only one feature left per dimension
        # input: batch x disp x height x width x 1
        # output: batch x disp x height x width
        cost = K.squeeze(cost, axis=-1)

        # calc propability for the disparities
        prob = Lambda(lambda c: -c)(cost)
        norm_prob = softmax(prob, axis=1)

        # calc disparity
        disp_vec = K.arange(0, max_disp, dtype='float32')
        disp_map = K.reshape(disp_vec, (1, 1, max_disp, 1))
        output = K.conv2d(
            norm_prob, disp_map, strides=(1, 1),
            data_format='channels_first', padding='valid',
        )
        return K.squeeze(output, axis=1)
    return Lambda(inner, output_shape=(height, width))


def build_model(height=None, width=None, max_disp=192) -> Model:
    left_input_layer = Input(shape=(height, width, 3), dtype='float32')
    right_input_layer = Input(shape=(height, width, 3), dtype='float32')

    # normalize between [-1;1]
    factor = np.float32(1) / 128.
    left_layer = Lambda(lambda x: x * factor - 1, output_shape=(height, width, 3))(left_input_layer)
    right_layer = Lambda(lambda x: x * factor - 1, output_shape=(height, width, 3))(right_input_layer)

    # unary Features with shared weights
    left_unary_features = unary_features_block(left_layer)
    UnaryFeaturesModel = Model(left_input_layer, left_unary_features)
    right_unary_features = UnaryFeaturesModel(right_layer)

    cost_volume = cost_volume_block(height, width, max_disp)([left_unary_features,
                                                              right_unary_features])
    cost = regularization_block(cost_volume)
    disparity_map = soft_argmin(height, width, max_disp)(cost)
    model = Model([left_input_layer, right_input_layer], disparity_map)

    def avgerr(y_true, y_pred):
        errors = K.abs(y_true - y_pred)
        return K.mean(errors)

    def bad(threshold, y_true, y_pred):
        errors = K.abs(y_true - y_pred)
        # numbers of errors in current batch
        nb_errors = K.sum(
            K.cast(
                K.greater_equal(errors, threshold),
                dtype='int32'
            )
        )
        # number of pixels per image
        nb_pixels = K.shape(y_true)[1] * K.shape(y_true)[2]
        # number of pixes in current batch
        nb_pixels = K.shape(errors)[0] * nb_pixels

        return nb_errors / nb_pixels

    bad05 = partial(bad, 0.5)
    bad05.__name__ = 'bad05'
    bad1 = partial(bad, 1)
    bad1.__name__ = 'bad1'
    bad2 = partial(bad, 2)
    bad2.__name__ = 'bad2'
    bad4 = partial(bad, 4)
    bad4.__name__ = 'bad4'

    model.compile(
        loss='mean_absolute_error',
        optimizer=RMSprop(lr=0.001),
        metrics=['accuracy', bad05, bad1, bad2, bad4, avgerr],
    )
    return model


def get_callbacks(model, initial_epoch, batch_size, iterations_per_epoch):
    return []


if __name__ == '__main__':
    import utils
    model = build_model(256, 512, max_disp=192)
    import pdb; pdb.set_trace()
    model.summary()
    memory_usage = utils.get_model_memory_usage(1, model)
    print(f"Required memory: {memory_usage}")
    print(model.layers[-1].output_shape)
