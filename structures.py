import math
import random

import attr
import numpy as np
import scipy.misc
from scipy.misc import toimage

import pfm


@attr.s
class StereoImage(object):
    left = attr.ib()
    right = attr.ib()
    disp = attr.ib()

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.height, self.width, _ = self.left.shape

        if not (self.left.shape == self.right.shape == self.disp.shape):
            raise ValueError('Image size does not match...')

    def random_crop(self, crop_height, crop_width):
        height, width, _ = self.left.shape
        crop_x = random.randint(0, width - crop_width)
        crop_y = random.randint(0, height - crop_height)

        left = self.left[crop_y:crop_y + crop_height,
                         crop_x:crop_x + crop_width]
        right = self.right[crop_y:crop_y + crop_height,
                           crop_x:crop_x + crop_width]
        disp = self.disp[crop_y:crop_y + crop_height,
                         crop_x:crop_x + crop_width]

        return StereoImage(left, right, disp)

    def normalize(self, divisor=64):
        height, width, _ = self.left.shape

        # rescale to multiple of `divisor`
        adapted_width = math.ceil(width / divisor) * divisor
        adapted_height = math.ceil(height / divisor) * divisor

        left = scipy.misc.imresize(self.left, (adapted_height, adapted_width))
        right = scipy.misc.imresize(self.right, (adapted_height, adapted_width))  # NOQA
        disp = scipy.misc.imresize(self.disp, (adapted_height, adapted_width))

        return StereoImage(left, right, disp)

    def resize(self, target_height, target_width):
        left = scipy.misc.imresize(self.left, (target_height, target_width))
        right = scipy.misc.imresize(self.right, (target_height, target_width))
        disp = scipy.misc.imresize(self.disp, (target_height, target_width))

        return StereoImage(left, right, disp)

    def show(self):
        toimage(self.left).show()
        toimage(self.right).show()
        toimage(self.disp).show()

    def stack_images(self):
        return np.concatenate((self.left, self.right), axis=2)


@attr.s
class StereoImageProxy(object):
    left_path = attr.ib()
    right_path = attr.ib()
    disparity_path = attr.ib()
    disp_transform = attr.ib(default=lambda d: d)

    @property
    def is_valid(self):
        return (self.left_path is not None and
                self.right_path is not None and
                self.disparity_path is not None)

    def _load_disp(self):
        if self.disparity_path.endswith('pfm'):
            disp, scale = pfm.load_pfm(self.disparity_path)
        else:
            disp = scipy.misc.imread(self.disparity_path)
        return self.disp_transform(disp)

    def load(self):
        if not self.is_valid:
            raise ValueError('Stereo and disparity image path are required.')

        left = scipy.misc.imread(self.left_path)
        right = scipy.misc.imread(self.right_path)
        disp = self._load_disp()

        return StereoImage(left, right, disp)
