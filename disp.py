import click
import cv2
import numpy as np
from scipy.stats import describe
from scipy.misc import toimage

import utils


@click.group()
def main():
    pass


@main.command()
@click.argument('disp_path', type=click.Path(exists=True, dir_okay=False))
def stats(disp_path):
    disp = utils.load_disp(disp_path)
    stats = describe(disp, axis=None)

    for attr in ['nobs', 'mean', 'variance', 'skewness', 'kurtosis']:
        print('{:10} {}'.format(attr, getattr(stats, attr)))
    print('{:10} {}'.format('min', stats.minmax[0]))
    print('{:10} {}'.format('max', stats.minmax[1]))


@main.command()
@click.option('--color', default=False, type=click.BOOL)
@click.argument('disp_path', type=click.Path(exists=True, dir_okay=False))
def show(disp_path, color):
    disp = utils.load_disp(disp_path)

    if color:
        # normalize to 0-1
        disp = (disp - disp.min()) / (disp.max() - disp.min())

        # HSL
        colored = np.ones((*disp.shape, 3), dtype=np.float32)
        colored[:, :, :] = 1  # TODO image should be completly white....
        colored[:, :, 0] = 360 * disp

        # HSL to RGB
        disp = cv2.cvtColor(colored, cv2.COLOR_HSV2RGB) * 255
    else:
        # normalize to 0-255
        scale = 255 / (disp.max() - disp.min())
        disp = (disp - disp.min()) * scale

    toimage(disp).show()


if __name__ == '__main__':
    main()
