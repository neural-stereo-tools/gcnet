import os
import random
import re
import signal
from itertools import cycle
from functools import partial

import click
import losswise
from losswise.libs import LosswiseKerasCallback
import numpy as np
from keras.callbacks import CSVLogger, ModelCheckpoint, TensorBoard
from more_itertools import chunked

import structures

losswise.set_api_key('gid3zyqfr')


class TupleIntType(click.ParamType):
    name = 'tuple(int)'

    def convert(self, value, param, ctx):
        try:
            v1, v2 = map(int, value.strip('()').split(','))
        except (IndexError, ValueError):
            self.fail(
                '{} is not a valid tuple of integer with size 2'.format(value))
        return (v1, v2)


def augment_image(image):
    from imgaug import augmenters as iaa
    if not hasattr(augment_image, 'indiviual'):
        indiviual_filters = [
            # fake minor vertical misscalibration
            iaa.Affine(translate_px={"y": (-1, 1)}),
            # change brightness of images (50-150% of original value)
            iaa.Multiply((0.5, 1.5)),
            # blur images with a sigma between 0 and 3.0
            iaa.GaussianBlur((0, 2.0)),
            # add gaussian noise to images
            iaa.AdditiveGaussianNoise(loc=0, scale=(0.0, 0.05*255),
                                      per_channel=0.5),
        ]
        aug = iaa.SomeOf((0, len(indiviual_filters)), indiviual_filters)
        aug.to_deterministic()
        augment_image.indiviual = aug

    if not hasattr(augment_image, 'general'):
        filters = [
            # change brightness of images (by -10 to 10 of original value)
            iaa.Add((-10, 10), per_channel=0.5),
            # improve or worsen the contrastcolo
            iaa.ContrastNormalization((0.5, 1.5), per_channel=0.5),
            # sharpen images
            iaa.Sharpen(alpha=(0, 1.0), lightness=(0.0, 0.75)),
            # change color
            iaa.InColorspace(to_colorspace="HSV", from_colorspace="RGB",
                             children=iaa.WithChannels(
                                 0, iaa.Add((-360, 360)))),
        ]
        aug = iaa.SomeOf((0, len(filters)), filters)
        aug.to_deterministic()
        augment_image.general = aug

    # apply autmentation to both images
    concat_img = np.concatenate((image.left, image.right), axis=1)
    concat_img = augment_image.general.augment_image(concat_img)

    # apply augenmation to each image individually
    height, width, _ = concat_img.shape
    left = concat_img[0:height, 0:int(width / 2)]
    right = concat_img[0:height, int(width / 2):width]

    image = structures.StereoImage(
        left=augment_image.indiviual.augment_image(left),
        right=augment_image.indiviual.augment_image(right),
        disp=image.disp,
    )
    return image


def infinite_training_gen(crop, random_size, images, n, augmentation=False, infinite=True):
    """
    Generates chunks of training data with size `n`. Returns a tuple
    with `n` input and output data.
    """
    batch_crop = None

    # load images on the fly and cycle them forever
    images_iter = iter(images)
    while True:
        input_data = list()
        expected_data = list()

        if crop:
            if random_size:
                batch_crop = (random.randint(256, crop[0]),
                              random.randint(256, crop[1]))
            else:
                batch_crop = crop

        for _ in range(n):
            # lazy load image infinitly
            try:
                image = next(images_iter)
            except StopIteration:
                if not infinite:
                    return
                images_iter = iter(images)
                image = next(images_iter)
            finally:
                image = image.load()

            if augmentation:
                image = augment_image(image)

            # in TF or numpy or scipy an image is (height, width, colors)
            if batch_crop:
                image = image.random_crop(*batch_crop)
            image = image.normalize()
            input_data.append((image.left, image.right))

            # height, width = image.disp.shape
            # disp = image.resize(int(height / 2), int(width / 2)).disp
            # disp = np.expand_dims(disp, axis=3)  # add third dimension
            expected_data.append(image.disp)

            # yield ([image.left, image.right], disp)

        
        data = (
            [
                np.asarray([d[0] for d in input_data]),
                np.asarray([d[1] for d in input_data]),
            ],
            np.asarray(expected_data),
        )

        with open('/mnt/c24f7164-762f-4df2-97c1-757364b2b5d3/alex/last_batch.npz', mode='wb') as fp:
            try:
                np.savez(fp, np.asarray(input_data), np.asarray(expected_data))
            except Exception as e:
                print(e)
                raise

        yield data


def validation_gen(images, n):
    """
    Generates chunks of validation data with size `n`. Returns a tuple
    with `n` input and output data.
    """
    for image_list in cycle(chunked(images, n)):
        input_data = list()
        expected_data = list()
        for image in image_list:
            image = image.load()
            image = image.normalize()

            # add images
            input_data.append((image.left, image.right))

            # height, width = image.disp.shape
            # disp = image.resize(int(height / 2), int(width / 2)).disp
            # disp = np.expand_dims(disp, axis=3)  # add third dimension
            expected_data.append(image.disp)

        # use images only if we have a valid set of size `n`
        yield (
            [
                np.asarray([d[0] for d in input_data], dtype='float32'),
                np.asarray([d[1] for d in input_data], dtype='float32'),
            ],
            np.asarray(expected_data, dtype='float32'),
        )


def load_images(sintel_path, flyingthings_path,
                validation=True, max_images=None, val_batch_size=None):
    print('> load images')
    images = []
    val_images = []
    for path, import_string in [(sintel_path, 'sintel'),
                                (flyingthings_path, 'flyingthings')]:
        if not path:
            continue

        print('>> {}'.format(import_string))
        dataset = __import__(import_string)
        train_imgs = list(
            dataset.collect_images(path, validation=False))
        print('>>> loaded {} images...'.format(len(train_imgs)))
        if validation:
            val_imgs = list(
                dataset.collect_images(path, validation=True))
            # limit to 500 images for now
            val_imgs = sorted(val_imgs)[:500]
            if val_batch_size:  # make it divisible by val_batch_size
                val_imgs = val_imgs[:int(len(val_imgs) / val_batch_size) * val_batch_size]  # NOQA
            print('>>> loaded {} images for validation...'.format(
                len(val_imgs)))
            val_images.extend(val_imgs)

        images.extend(train_imgs)

    if not images:
        raise ValueError("Please provide at least one dataset.")

    random.shuffle(images)

    if not validation:
        val_images = None

    if max_images:
        images = images[:max_images]

    return images, val_images


def get_callbacks(network, output_dir, validation=True):
    # model checkpoint
    if validation:
        checkpont_filename = 'weights_epoch{epoch:02d}_loss{val_loss:.2f}.hdf5'
    else:
        checkpont_filename = 'weights_epoch{epoch:02d}.hdf5'

    model_checkpoint = ModelCheckpoint(
        os.path.join(output_dir, checkpont_filename),
        save_weights_only=True,
        period=2,
    )
    csv_logger = CSVLogger(
        os.path.join(output_dir, 'training.csv'),
        append=True,
    )
    tensorboard = TensorBoard(log_dir=os.path.join(output_dir, 'logs'), histogram_freq=10 if validation else 0, write_graph=True, write_images=True)
    callbacks = [model_checkpoint, csv_logger,
                 tensorboard]

    return callbacks


def train(network, weights_path,  images, val_images, training_gen, output_dir,
          batch_size=10, val_batch_size=10, epochs=10, initial_epoch=0,
          validation=True, img_size=None):
    print('> build model')
    img_size = img_size or (None, None)
    model = network.build_model(*img_size)

    # load weights if provided
    if weights_path:
        print('>> load weights from `{}`'.format(weights_path.name))
        model.load_weights(weights_path.name)

    # initialize training set
    nb_images_per_epoch = 2000
    nb_images_per_validation = min(len(val_images or []), 20)
    nb_training = len(images)
    nb_validation = len(val_images or [])
    iterations_per_epoch = int(
        min(nb_images_per_epoch, nb_training) / batch_size)
    iterations_per_validation = int(
        min(nb_images_per_validation, nb_validation) / val_batch_size)

    print('\n=========================')
    print('Training data')
    print('=========================')
    print('Total images: {}'.format(nb_training + nb_validation))
    print('Validation images: {}'.format(nb_validation))
    print('Training images: {}'.format(nb_training))
    print('Images per batch: {}'.format(batch_size))
    print('Batches per epoch: {}'.format(iterations_per_epoch))
    print('Total images per epoch: {}'.format(nb_images_per_epoch))
    print('Total validation images per epoch: {}'.format(
        nb_images_per_validation))
    print('=========================\n')

    # stop training on ctrl+c
    def signal_handler(signal, frame):
        model.stop_training = True
        print('\n!!! Training will be stopped after current epoch!\n')
    signal.signal(signal.SIGINT, signal_handler)

    print('\nPress Ctrl+C to stop training after the current epoch.\n'
          'Your model will be saved to the current directory.\n\n')

    callbacks = get_callbacks(network, output_dir, validation=validation)
    callbacks.extend(network.get_callbacks(model,
                                           initial_epoch,
                                           batch_size,
                                           iterations_per_epoch))

    fit_args = (training_gen(images, batch_size),)
    fit_kwargs = {
        'steps_per_epoch': iterations_per_epoch,
        'epochs': epochs,
        'initial_epoch': initial_epoch,
        'callbacks': callbacks,
        'verbose': 1,
    }

    if val_images:
        fit_kwargs.update({
            'validation_data': training_gen(val_images, val_batch_size, infinite=False),
            'validation_steps': iterations_per_validation,
        })

    # fit the network
    model.fit_generator(*fit_args, **fit_kwargs)

    return model


def limit_mem():
    import keras.backend as K

    K.get_session().close()
    cfg = K.tf.ConfigProto()
    cfg.gpu_options.allow_growth = True
    K.set_session(K.tf.Session(config=cfg))


@click.command()
@click.option('--batch-size', default=10, type=click.INT)
@click.option('--val-batch-size', default=10, type=click.INT)
@click.option('--epochs', default=100, type=click.INT)
@click.option('--max_images', default=None, type=click.INT)
@click.option('--weights_path', type=click.File())
@click.option('--crop', type=TupleIntType())
@click.option('--random_crop', default=False, type=click.BOOL)
@click.option('--validation', default=True, type=click.BOOL)
@click.option('--sintel_path', type=click.Path(exists=True, file_okay=False))
@click.option('--flyingthings_path', type=click.Path(exists=True,
                                                     file_okay=False))
@click.argument('network_name', type=click.Choice(['dispnet', 'gcnet']))
@click.argument('output_dir', type=click.Path(exists=True, file_okay=False))
def run(batch_size, val_batch_size, epochs, max_images, crop, random_crop,
        validation,
        weights_path, sintel_path, flyingthings_path,
        network_name, output_dir):

    images, val_images = load_images(sintel_path, flyingthings_path,
                                     validation=validation,
                                     val_batch_size=val_batch_size,
                                     max_images=max_images)

    # only on TF
    # limit_mem()  # incremental gpu usage

    # load neural network
    network = __import__(network_name)

    # extract epoch from filename, `weights` have higher precedence
    epoch_filename = weights_path
    initial_epoch = 0
    if epoch_filename:
        match = re.search(r'epoch(\d+)', epoch_filename.name)
        if match:
            initial_epoch = int(match.groups()[0])
            print('>> continue at epoch {}'.format(initial_epoch))
        else:
            raise ValueError('Could noch extract epoch from filename.')

    print('> run training')
    model = train(network, weights_path,
                  images, val_images,
                  partial(infinite_training_gen, crop, random_crop),
                  output_dir,
                  batch_size=batch_size,
                  val_batch_size=val_batch_size,
                  epochs=epochs,
                  initial_epoch=initial_epoch,
                  validation=validation,
                  img_size=crop)

    # store current state
    model_filename = os.path.join(output_dir, 'model.json')
    weights_filename = os.path.join(output_dir, 'weights.hdf5')
    model_state_filename = os.path.join(output_dir, 'model.hdf5')

    model.save(model_state_filename)
    model.save_weights(weights_filename)
    with open(model_filename, 'w') as f:
        try:
            f.write(model.to_json())
        except Exception as e:
            print('Could not save model as json...')
            print(e)

    print('Model stored as `{}`, `{}`, `{}`.'.format(
        model_filename,
        weights_filename,
        model_state_filename,
    ))


if __name__ == '__main__':
    import sys
    sys.setrecursionlimit(10000)
    run()
