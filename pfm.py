# Inpired from
# https://gist.github.com/chpatrick/8935738
#
import re
import sys

import numpy as np


def load_pfm(path):
    """
    Load a PFM file into a Numpy array. Note that it will have
    a shape of H x W, not W x H. Returns a tuple containing the
    loaded image and the scale factor from the file.
    """
    fh = open(path, 'rb')

    color = None
    width = None
    height = None
    scale = None
    endian = None

    header = fh.readline().rstrip().decode('ascii')
    if header == 'PF':
        color = True
    elif header == 'Pf':
        color = False
    else:
        raise Exception('Not a PFM fh.')

    dim_match = re.match(r'^(\d+)\s(\d+)\s$', fh.readline().decode('ascii'))
    if dim_match:
        width, height = map(int, dim_match.groups())
    else:
        raise Exception('Malformed PFM header.')

    scale = float(fh.readline().rstrip().decode('ascii'))
    if scale < 0:  # little-endian
        endian = '<'
        scale = -scale
    else:
        endian = '>'  # big-endian

    data = np.fromfile(fh, endian + 'f')
    shape = (height, width, 3) if color else (height, width)

    data = np.reshape(data, shape)
    data = np.flipud(data)
    return data, scale


def save_pfm(path, image, scale=1):
    """
    Save a Numpy array to a PFM file.
    """
    fh = open(path, 'wb')

    color = None

    if image.dtype.name != 'float32':
        raise Exception('Image dtype must be float32.')

    image = np.flipud(image)

    if len(image.shape) == 3 and image.shape[2] == 3:
        # color image
        color = True
    elif (len(image.shape) == 2 or len(image.shape) == 3 and
            image.shape[2] == 1):
        # greyscale
        color = False
    else:
        raise Exception(
            'Image must have H x W x 3, H x W x 1 or H x W dimensions.')

    fh.write(b'PF\n' if color else b'Pf\n')
    fh.write('{:d} {:d}\n'.format(
        image.shape[1], image.shape[0]).encode('ascii'))

    endian = image.dtype.byteorder

    if endian == '<' or endian == '=' and sys.byteorder == 'little':
        scale = -scale

    fh.write('{:f}\n'.format(scale).encode('ascii'))

    image.tofile(fh)
